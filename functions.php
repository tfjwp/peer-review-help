<?php
/**
 * WP-Starter functions and definitions
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage WP_Starter
 * @since WP-Starter 4.0
 */

/**
 * Enqueue our child-theme style sheets
 */
function wpstarter_child_style() {
    wp_dequeue_style('wpforge');
    wp_enqueue_style('parent-styles', get_template_directory_uri() . '/style.css', '', '5.5.2.5');
    wp_enqueue_style('child-styles', get_stylesheet_uri(), array( 'parent-styles' ), '4.0');
}
add_action( 'wp_enqueue_scripts', 'wpstarter_child_style');

?>